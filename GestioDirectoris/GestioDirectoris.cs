﻿/*
* AUTHOR: Angel Navarrete Sanchez
* DATE: 2023/01/24
* DESCRIPTION:  Exercicis de Gestio de Fitxers
*/
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;

namespace GestioDirectoris
{
    class GestioDirectoris
    {
        static void Main(string[] args)
        {
            //string directori = @"E:\Angel Navarrete\";
            GestioDirectoris app = new GestioDirectoris();
            app.Menu();

        }

        public void Menu()
        {
            MostrarOpcionsMenu();
            string opcio = DemanarOpcioMenu();
            EscollirOpcioMenu(opcio);
        }
        public void MostrarOpcionsMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                                         Exercicis Gestio Directoris Angel Navarrete");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("\t1.- Exercici 1: Comprovar Fitxer");
            Console.WriteLine("\t2.- Exercici 2: Llistar Carpetes");
            Console.WriteLine("\t3.- Exercici 3: Modificar Directori");
            Console.WriteLine("\t4.- Exercici 4: Llistar Tot d'un directori");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public string DemanarOpcioMenu()
        {
            Console.Write("Escull una opció: ");
            string opcio = Console.ReadLine();
            return opcio;
        }

        public void EscollirOpcioMenu(string opcio)
        {
            do
            {
                switch (opcio)
                {
                    case "1":
                        Exercici1(DemanarDirectori());
                        break;
                    case "2":
                        Exercici2();
                        break;
                    case "3":
                        Exercici3();
                        break;
                    case "4":
                        Exercici4();
                        break;

                    case "0":
                        Console.WriteLine("Adeu");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opcio Incorrecta");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                }
                Console.ReadLine();
                Console.Clear();
                MostrarOpcionsMenu();
                opcio = DemanarOpcioMenu();
            } while (opcio != "0");
        }

        string DemanarDirectori()
        {
            Console.Write("Introdueix un directori: ");
            string path = Console.ReadLine();
            return path;
        }
        /*DESCRIPTION : Demanem a l’usuari el nom d’un fitxer o d’una carpeta. Cal indicar si existeix o no. En cas d’existir indicar si es tracta d’un fitxer o d’un directori, i mostrar la ruta absoluta.*/
        void Exercici1(string directori)
        {
            Console.Write("Introdueix un nombre del fixer o directori que vols cercar: ");
            string nom = Console.ReadLine();
            string path = directori + nom;


            if (Directory.Exists(path))
            {
                Console.WriteLine("Existeix el directori amb el nom: " + nom + " a " + Path.GetFullPath(path));
            }
            else if (File.Exists(path))
            {
                Console.WriteLine("Existeix l'archiu amb el nom: " + nom + " a " + Path.GetFullPath(path));
            }
            else
            {
                Console.WriteLine("No existeix un archiu o directori amb el nom " + nom);
            }
        }
        /*DESCRIPTION: Demanem a l’usuari un directori, hem de treure el llistat de les carpetes i fitxers del directori, com a cadenes, com a carpetes i com a fitxers, indicant si es tracta d’un directori <DIR>, o d’un fitxer <FILE>.*/
        void Exercici2()
        {
            string path = DemanarDirectori();
            //string path = @"E:\";

            DirectoryInfo carpeta = new DirectoryInfo(path);

            IEnumerable<DirectoryInfo> carpetes = carpeta.EnumerateDirectories();
            IEnumerable<FileInfo> fitxers = carpeta.EnumerateFiles("*.txt", SearchOption.TopDirectoryOnly);
            foreach (var carpetaDirectori in carpetes)
            {
                Console.WriteLine("<DIR> "+ carpetaDirectori);
            }
            foreach (var fitxer in fitxers)
            {
                Console.WriteLine("<FILE> " + fitxer);
            }

        }
        /*DESCRIPTION: Demanem a l’usuari un directori origen, hem de poder crear un directori , esborrar-lo i/o canviar-li el nom.*/
        void Exercici3()
        {
            //string path = DemanarDirectori();
            string path = @"E:\";
            int opcio = 1;
            do
            {
                Console.WriteLine("Que vols fer:\n1.CREAR DIRECTORI\n2.ELIMINAR DIRECTORI\n3.CAMBIAR NOM A UN DIRECTORI\n0.Exit");
                opcio = Convert.ToInt32(Console.ReadLine());
                switch (opcio)
                {
                    case 1:
                        CrearDirectori(path);
                        break;
                    case 2:
                        EliminarDirectori(path);
                        break;
                    case 3:
                        CambiarNom(path);
                        break;
                    case 0:
                        Console.WriteLine("Adios");
                        break;
                    default:
                        Console.WriteLine("Error");
                        break;
                }
                Console.ReadLine();
                Console.Clear();
            }
            while (opcio != 0);

        }
        private void CrearDirectori(string path)
        {
            string directori = DemanarDirectori();
            string pathComplert = path + directori;
            Directory.CreateDirectory(directori);
            if (Directory.Exists(pathComplert))
            {
                Console.WriteLine("El directori ja existeix.");
            }
            else
            {
                DirectoryInfo carpeta = Directory.CreateDirectory(pathComplert);
            }
        }

        private void CambiarNom(string path)
        {
            
            string directori = DemanarDirectori();
            Console.WriteLine("Introdueix el nou nom: ");
            string directoriNouNom = Console.ReadLine();

            if (Directory.Exists((path+directori)))
            {
                FileSystem.Rename((path + directori), (path + directoriNouNom));
            }
            else
            {
                Console.WriteLine("El directori no existeix");
            }

        }

        private void EliminarDirectori(string path)
        {
            string directori = DemanarDirectori();
            string pathComplert = path + directori;

            if (Directory.Exists(pathComplert))
            {
                Directory.Delete(pathComplert);
            }
            else
            {
                Console.WriteLine("El directori no existeix.");
            }
        }

        

        /*DESCRIPTION: Demanem  a l’usuari un directori origen, i mostrem per pantalla el llistat  de carpetes i fitxers que conté, el fitxers i directoris de les  carpetes, com si s’executa un dir/s. Dels fitxers cal mostrar el nom, la mida, la data (format data de l’última modificació). De les carpetes el nom, que es tracta d’un directori i  la data(format data de l’última modificació)*/
        void Exercici4() {
            string path = DemanarDirectori();
            //string path = @"E:\";

            DirectoryInfo directory = new DirectoryInfo(path);
            if (directory.Exists)
            {
                Console.WriteLine("\nDirectoris:");
                DirectoryInfo[] directoris = directory.GetDirectories();
                foreach (DirectoryInfo directori in directoris)
                {
                    Console.WriteLine("\t<DIR> " + directori.Name + " " + directori.LastWriteTime);
                }
                Console.WriteLine("Fitxers:");
                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo file in files)
                {
                    Console.WriteLine("\t <FILE> " + file.Name + " (" + file.Length / 1024 + "KB) - " + file.LastWriteTime);
                }
            }
            else
            {
                Console.WriteLine("The specified directory does not exist.");
            }


            
        } 
    }
}
